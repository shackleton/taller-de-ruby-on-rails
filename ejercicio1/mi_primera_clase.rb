#!/usr/bin/env ruby

# Comentario en Ruby

class Vehiculo
  @@velocidad_max = 120
  @@velocidad_urbana = 60

  attr_reader :velocidad_actual

  def initialize(velocidad_actual = 0)
    @velocidad_actual = velocidad_actual
  end

  def tipo_de_velocidad
    case @velocidad_actual
    when 0
      puts "Estacionado"
    when 0..@@velocidad_urbana
      puts "Velocidad Urbana"
    when @@velocidad_urbana..@velocidad_max
      puts "Velocidad de Carretera"
    else
      puts "Exceso de Velocidad"
    end
  end

  def acelerar()
    @velocidad_actual  += 10
    @velocidad_actual = @@velocidad_max if @velocidad_actual > @@velocidad_max
  end

  def frenar()
    @velocidad_actual -= 10
    @velocidad_actual = 0 unless @velocidad_actual >= 0
  end


  Vehiculo.new(10)
