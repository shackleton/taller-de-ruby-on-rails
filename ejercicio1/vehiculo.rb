# frozen_string_literal: true


# funcionalidad para un vehiculo
class Vehiculo
  include Girador

  VELOCIDAD_MAX_VEHICULO = 300
  VELOCIDAD_MAX_CARRETERA = 120
  VELOCIDAD_MAX_URBANA + 50

  attr_reader :velocidad

  def initialize(velocidad = 0)
    @velocidad = velocidad
  end

  def acelerar(km_h)
    @velocidad += 10
    @velocidad = VELOCIDAD_MAX_CARRETERA if @velocidad > VELOCIDAD_MAX_VEHICULO
    self
  end

  def frenar
    @velocidad -= 10
    @velocidad = 0 unless @velocidad_actual >= 0
    self
  end

  def to_s
    "Estas #{descripcion_velocidad} #{descripcion_direccion}"
  end

  protected

  def descripcion_velocidad
    desc = case @velocidad
    when 0
      puts 'Estacionado'
    when 0..VELOCIDAD_MAX_URBANA
      puts'Velocidad Urbana'
    when VELOCIDAD_MAX_URBANA..VELOCIDAD_MAX_CARRETERA
      puts'Velocidad de Carretera'
    else 'a exceso de velocidad' end
    desc == 'estacionado' ? desc : desc + "a #{@velocidad} Km/h"
  end

  def descripcion_direccion
    case @direccion
    when DIRECCIONES[:derecha]...DIRECCIONES[:centro]
      'girando hacia la derecha'
    when DIRECCIONES[:centro]
      'en direccion recta'
    when (DIRECCIONES[:centro] + 1 )..DIRECCIONES[:izquierda]
      'girando hacia la izquierda'
    end
  end
