# frozen_string_literal: true
# Algoritmo para girar en 180 grados

module Girador
  attr_reader :direccion

  DIRECCIONES = { derecha: 0, centro: 90, izquierda: 180 }.freeze
  private_constant :DIRECCIONES

  def girar(direccion = :centro)
    if DIRECCIONES[direccion]
      @direccion = DIRECCIONES[direccion]
    elsif direccion >= 0 && direccion <= 180
      @direccion = direccion
    else
      raise ArgumentError, 'Giro invalido'
    end
  rescue ArgumentError
    print 'Giro invalido, puede ser de 0 a 180'
    puts 'o un simbolo :izquierda, :derecha o :centro'
  end
end
